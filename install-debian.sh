sudo yum remove -y docker docker-engine docker.io containerd runc
sudo yum update
sudo yum install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

OS=$(cat /etc/os-release | grep -wPo "ID=\K(.*)")
curl -fsSL https://download.docker.com/linux/$OS/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(lsb_release -is | awk '{print tolower($0)}') $(lsb_release -cs) stable"

sudo yum update
sudo yum install -y docker-ce docker-ce-cli containerd.io
