#!/usr/bin/env bash

set -e
set -u

# decrypt the private key

# set the git username/email to be able to perform git operations
echo git version: $(git --version)
echo "${SSH_PRIVATE_KEY}" > ./gitlab_private_key
chmod 400 ./gitlab_private_key
git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"

# set the push URL differently to leverage SSH protocol
git remote set-url --push origin git@gitlab.com:corentin.normand/docker-install.git
git remote -v

# set the git tag to the current version

git tag -a $REVISION -m "Setting version as tag during build."

# push the git tag
# leverage GIT_SSH option to use a dedicated SSH key
# see https://git-scm.com/docs/git#git-codeGITSSHcode for documentation of this feature
echo 'ssh -i ./gitlab_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $*' > ssh
chmod +x ssh
GIT_SSH='./ssh' git push origin $REVISION